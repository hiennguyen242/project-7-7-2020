package com.banhcanhvn.customer.utils.api.service;

import com.banhcanhvn.customer.json.AllMerchantByNearResponseJson;
import com.banhcanhvn.customer.json.AllMerchantbyCatRequestJson;
import com.banhcanhvn.customer.json.AllTransResponseJson;
import com.banhcanhvn.customer.json.BankResponseJson;
import com.banhcanhvn.customer.json.BeritaDetailRequestJson;
import com.banhcanhvn.customer.json.BeritaDetailResponseJson;
import com.banhcanhvn.customer.json.ChangePassRequestJson;
import com.banhcanhvn.customer.json.DetailRequestJson;
import com.banhcanhvn.customer.json.EditprofileRequestJson;
import com.banhcanhvn.customer.json.GetAllMerchantbyCatRequestJson;
import com.banhcanhvn.customer.json.GetFiturResponseJson;
import com.banhcanhvn.customer.json.GetHomeRequestJson;
import com.banhcanhvn.customer.json.GetHomeResponseJson;
import com.banhcanhvn.customer.json.GetMerchantbyCatRequestJson;
import com.banhcanhvn.customer.json.LoginRequestJson;
import com.banhcanhvn.customer.json.LoginResponseJson;
import com.banhcanhvn.customer.json.MerchantByCatResponseJson;
import com.banhcanhvn.customer.json.MerchantByIdResponseJson;
import com.banhcanhvn.customer.json.MerchantByNearResponseJson;
import com.banhcanhvn.customer.json.MerchantbyIdRequestJson;
import com.banhcanhvn.customer.json.PrivacyRequestJson;
import com.banhcanhvn.customer.json.PrivacyResponseJson;
import com.banhcanhvn.customer.json.PromoRequestJson;
import com.banhcanhvn.customer.json.PromoResponseJson;
import com.banhcanhvn.customer.json.RateRequestJson;
import com.banhcanhvn.customer.json.RateResponseJson;
import com.banhcanhvn.customer.json.RegisterRequestJson;
import com.banhcanhvn.customer.json.RegisterResponseJson;
import com.banhcanhvn.customer.json.ResponseJson;
import com.banhcanhvn.customer.json.SearchMerchantbyCatRequestJson;
import com.banhcanhvn.customer.json.TopupRequestJson;
import com.banhcanhvn.customer.json.TopupResponseJson;
import com.banhcanhvn.customer.json.WalletRequestJson;
import com.banhcanhvn.customer.json.WalletResponseJson;
import com.banhcanhvn.customer.json.WithdrawRequestJson;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Ourdevelops Team on 10/13/2019.
 */

public interface UserService {

    @POST("pelanggan/login")
    Call<LoginResponseJson> login(@Body LoginRequestJson param);

    @POST("pelanggan/kodepromo")
    Call<PromoResponseJson> promocode(@Body PromoRequestJson param);

    @POST("pelanggan/listkodepromo")
    Call<PromoResponseJson> listpromocode(@Body PromoRequestJson param);

    @POST("pelanggan/list_bank")
    Call<BankResponseJson> listbank(@Body WithdrawRequestJson param);

    @POST("pelanggan/changepass")
    Call<LoginResponseJson> changepass(@Body ChangePassRequestJson param);

    @POST("pelanggan/register_user")
    Call<RegisterResponseJson> register(@Body RegisterRequestJson param);

    @GET("pelanggan/detail_fitur")
    Call<GetFiturResponseJson> getFitur();

    @POST("pelanggan/forgot")
    Call<LoginResponseJson> forgot(@Body LoginRequestJson param);

    @POST("pelanggan/privacy")
    Call<PrivacyResponseJson> privacy(@Body PrivacyRequestJson param);

    @POST("pelanggan/home")
    Call<GetHomeResponseJson> home(@Body GetHomeRequestJson param);

    @POST("pelanggan/topupstripe")
    Call<TopupResponseJson> topup(@Body TopupRequestJson param);

    @POST("pelanggan/withdraw")
    Call<ResponseJson> withdraw(@Body WithdrawRequestJson param);

    @POST("pelanggan/topuppaypal")
    Call<ResponseJson> topuppaypal(@Body WithdrawRequestJson param);

    @POST("pelanggan/rate_driver")
    Call<RateResponseJson> rateDriver(@Body RateRequestJson param);

    @POST("pelanggan/edit_profile")
    Call<RegisterResponseJson> editProfile(@Body EditprofileRequestJson param);

    @POST("pelanggan/wallet")
    Call<WalletResponseJson> wallet(@Body WalletRequestJson param);

    @POST("pelanggan/history_progress")
    Call<AllTransResponseJson> history(@Body DetailRequestJson param);

    @POST("pelanggan/detail_berita")
    Call<BeritaDetailResponseJson> beritadetail(@Body BeritaDetailRequestJson param);

    @POST("pelanggan/all_berita")
    Call<BeritaDetailResponseJson> allberita(@Body BeritaDetailRequestJson param);

    @POST("pelanggan/merchantbykategoripromo")
    Call<MerchantByCatResponseJson> getmerchanbycat(@Body GetMerchantbyCatRequestJson param);

    @POST("pelanggan/merchantbykategori")
    Call<MerchantByNearResponseJson> getmerchanbynear(@Body GetMerchantbyCatRequestJson param);

    @POST("pelanggan/allmerchantbykategori")
    Call<AllMerchantByNearResponseJson> getallmerchanbynear(@Body GetAllMerchantbyCatRequestJson param);

    @POST("pelanggan/itembykategori")
    Call<MerchantByIdResponseJson> getitembycat(@Body GetAllMerchantbyCatRequestJson param);

    @POST("pelanggan/searchmerchant")
    Call<AllMerchantByNearResponseJson> searchmerchant(@Body SearchMerchantbyCatRequestJson param);

    @POST("pelanggan/allmerchant")
    Call<AllMerchantByNearResponseJson> allmerchant(@Body AllMerchantbyCatRequestJson param);

    @POST("pelanggan/merchantbyid")
    Call<MerchantByIdResponseJson> merchantbyid(@Body MerchantbyIdRequestJson param);


}
